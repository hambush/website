---
title: Hambush
---

# Hambush {: .hidden }

![Hambush Logo](static/logo.svg)

## Foreword

> First EP available on March 11, 2020 on all streaming platforms and digital stores

[:fontawesome-brands-youtube:{: style="transform: scale(2.0); margin: 1rem;" }](https://music.youtube.com/playlist?list=OLAK5uy_lsy3Mfn1nGpkthmCpHLvFcFlklkLxRexo "YouTube Music")
[:fontawesome-brands-spotify:{: style="transform: scale(2.0); margin: 1rem;" }](https://open.spotify.com/album/1dGvxvcEvniIUNpGBSWEbS?si=nhN9AlECRxu8gBhQUkxfHA "Spotify")
[:fontawesome-brands-deezer:{: style="transform: scale(2.0); margin: 1rem;" }](https://www.deezer.com/album/134603952 "Deezer")
[:fontawesome-brands-apple:{: style="transform: scale(2.0); margin: 1rem;" }](https://music.apple.com/nl/album/foreword-feat-david-benoist-ep/1502102988?app=music "Apple Music")
[:fontawesome-brands-amazon:{: style="transform: scale(2.0); margin: 1rem;" }](https://music.amazon.com/albums/B085G2T38B "Amazon Music")
[:fontawesome-brands-soundcloud:{: style="transform: scale(2.0); margin: 1rem;" }](https://soundcloud.com/user-567690492/sets/foreword "SoundCloud")

## Video

> Watch the clip of "Dusk", from the "Foreword" EP

<iframe
    width="600"
    height="340"
    title="Hambush - Dusk (Official Clip)"
    src="https://www.youtube-nocookie.com/embed/qiMO9QPeqtQ"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
></iframe>

## Audio

> Listen to the "Foreword" EP

<iframe
    width="600"
    height="340"
    title="Hambush - Foreword EP"
    src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1014360886&color=%23fb151b&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=false&visual=false"
    frameborder="0"
    allowtransparency="true"
    allow="encrypted-media"
></iframe>

## Press

> We're talking about us on the web

[![Music'n'Gre](static/press/musicngre.png){ align=left style="width: 64px;" }][music-n-gre-2020]

[**Music'n'Gre**][music-n-gre-2020] | Apr 8, 2020 <br>
Interview <br>
_Marre des pochettes d'album de métal sur fond sombre_ <br>

[music-n-gre-2020]: https://www.musicngre.fr/hambush-grenoble-interview/ "Marre des pochettes d'album de métal sur fond sombre"

## Band

> Who is Hambush?

**Chris Mamola** Guitars <br>
**Romain Clement** Bass <br>
**Mickael Cottin-Bizonne** Drums <br>
**David Benoist** Vocals <br>

## Contact

> You can contact us directly by e-mail and with common social networks

[:fontawesome-solid-at:{: style="transform: scale(2.0); margin: 1rem;" }](mailto:hambush.music@gmail.com "E-mail")
[:fontawesome-brands-facebook:{: style="transform: scale(2.0); margin: 1rem;" }](https://www.facebook.com/Hambush-461004794678060/ "Facebook")
[:fontawesome-brands-instagram:{: style="transform: scale(2.0); margin: 1rem;" }](https://www.instagram.com/hambush.music/ "Instagram")
