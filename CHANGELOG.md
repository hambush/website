# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.1] - 2021-09-23
### Added
- Add `robots.txt` static template for better SEO

### Changed
- Update dependencies

## [0.5.0] - 2021-09-03
### Changed
- Migrate from Node.js to Python
- Migrate from Nuxt.js to MkDocs

## [0.4.6] - 2020-12-30
### Fixed
- GitLab icon size in credits

### Changed
- Update and upgrade dependencies
- Rename all Zeit Now references to Vercel

## [0.4.5] - 2020-07-10
### Changed
- Update and upgrade dependencies

## [0.4.4] - 2020-05-24
### Changed
- Update and upgrade dependencies
- Set npm version to 6.14.5

## [0.4.3] - 2020-05-16
### Fixed
- Production deployment with Now

### Changed
- Set Node.js version to 12.16.3

## [0.4.2] - 2020-05-16
### Changed
- Update and upgrade dependencies
- Production deployment using Now

## [0.4.1] - 2020-04-22
### Fixed
- Update Chiffre Analytics events push URL (cf. `nuxt-chiffre@0.2.1`)

### Changed
- Update dependencies

## [0.4.0] - 2020-04-12
### Added
- Navigation bar at the top
- Press section with articles

### Changed
- Update dependencies

### Fixed
- Trailing whitespaces with i18n component interpolation

## [0.3.2] - 2020-04-05
### Added
- Sentry crash reporting support

### Changed
- App icon to square background

## [0.3.1] - 2020-03-20
### Added
- Enable PWA workbox

### Fixed
- Link color contrast

## [0.3.0] - 2020-03-19
### Changed
- Hide visual banner in SoundCloud audio player

### Fixed
- Make audio player visible on all screen sizes

## [0.2.5] - 2020-03-18
### Added
- SoundCloud link to listening platforms

### Changed
- Use SoundCloud as embedded audio player

## [0.2.4] - 2020-03-15
### Added
- Instagram link to contact section

## [0.2.3] - 2020-03-13
### Added
- [Chiffre](https://chiffre.io) analytics usage notice

### Changed
- Use external [nuxt-chiffre](https://www.npmjs.com/package/nuxt-chiffre) module

## [0.2.2] - 2020-03-12
### Added
- Analytics tracking using [Chiffre](https://chiffre.io)

## [0.2.1] - 2020-03-11
### Added
- More links to listening and buying platforms

## [0.2.0] - 2020-03-11
### Added
- Foreword album links for all major platforms

## [0.1.3] - 2020-03-10
### Added
- Deezer link
- Error page fallback

### Changed
- Update homepage
- Update credits
- Update dependencies
- Set Node.js version to 12.16.1

## [0.1.2] - 2020-03-08
### Added
- Automatic CNAME file generation

### Changed
- Update homepage main section
- PWA app color and icon

## [0.1.1] - 2020-03-08
### Added
- More links and icons to the music stores

### Fixed
- Logo in SVG format
- Router base URL with optional static prefix

## [0.1.0] - 2020-03-08
### Added
- Initial release of Hambush website

[unreleased]: https://gitlab.com/hambush/website/compare/0.5.1...master
[0.5.1]: https://gitlab.com/hambush/website/compare/0.5.0...0.5.1
[0.5.0]: https://gitlab.com/hambush/website/compare/0.4.6...0.5.0
[0.4.6]: https://gitlab.com/hambush/website/compare/0.4.5...0.4.6
[0.4.5]: https://gitlab.com/hambush/website/compare/0.4.4...0.4.5
[0.4.4]: https://gitlab.com/hambush/website/compare/0.4.3...0.4.4
[0.4.3]: https://gitlab.com/hambush/website/compare/0.4.2...0.4.3
[0.4.2]: https://gitlab.com/hambush/website/compare/0.4.1...0.4.2
[0.4.1]: https://gitlab.com/hambush/website/compare/0.4.0...0.4.1
[0.4.0]: https://gitlab.com/hambush/website/compare/0.3.2...0.4.0
[0.3.2]: https://gitlab.com/hambush/website/compare/0.3.1...0.3.2
[0.3.1]: https://gitlab.com/hambush/website/compare/0.3.0...0.3.1
[0.3.0]: https://gitlab.com/hambush/website/compare/0.2.5...0.3.0
[0.2.5]: https://gitlab.com/hambush/website/compare/0.2.4...0.2.5
[0.2.4]: https://gitlab.com/hambush/website/compare/0.2.3...0.2.4
[0.2.3]: https://gitlab.com/hambush/website/compare/0.2.2...0.2.3
[0.2.2]: https://gitlab.com/hambush/website/compare/0.2.1...0.2.2
[0.2.1]: https://gitlab.com/hambush/website/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/hambush/website/compare/0.1.3...0.2.0
[0.1.3]: https://gitlab.com/hambush/website/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/hambush/website/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/hambush/website/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/hambush/website/tree/0.1.0
