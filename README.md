# Hambush Website

> Official website for Hambush music band

[![Pipeline Status](https://gitlab.com/hambush/website/badges/master/pipeline.svg)](https://gitlab.com/hambush/website/-/pipelines/latest)

## Build Setup

``` bash
# install dependencies
$ pipenv install -d

# serve with hot reload at localhost:8000
$ mkdocs serve

# build for production
$ mkdocs build
```

For detailed explanation on how things work, check out [MkDocs docs](https://www.mkdocs.org).

## License

Licensed under GNU Affero General Public License v3.0 (AGPLv3)

Copyright (c) 2020 - present  Hambush